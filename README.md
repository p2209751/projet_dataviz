# Projet_DataViz

## Membres
COGONI Guillaume   
YE Gildas    
MATHIEU Corentin      
ALHERITHIERE Héloïse    


I.apport_au_pib_en_pourcentage_de_certaines_ressources_Data.csv :
* Coal rents (% of GDP)
* Mineral rents (% of GDP)
* Oil rents (% of GDP)
* Forest rents (% of GDP)
* Natural gas rents (% of GDP)
* Total natural resources rents (% of GDP)

II. Pays_CO2_EMISSION :
* CO2 emissions (kt)

III. poucentage_consommation_energy_renouvelable
* Renewable energy consumption (% of total final energy consumption)

IV. import_net_pourcentage_utilisation
* Energy imports, net (% of energy use)

V. Production_Renouvelable_nucleaire_fossile
* Electricity production from nuclear sources (% of total)
* Electricity production from oil, gas and coal sources (% of total)
* Electricity production from renewable sources, excluding hydroelectric (% of total)

VI.Population_in_each_country_by_year
* Population, total